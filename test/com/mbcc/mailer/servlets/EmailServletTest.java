package com.mbcc.mailer.servlets;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

import com.mbcc.mailer.data.DerbyDao;

public class EmailServletTest {

	@Test
	public void validEmailIsInserted() {
		EmailDTO email = new EmailDTO("email subject", "testing eamil", "Alumni", "jravi");

		EmailServlet servlet = new EmailServlet();
		DerbyDao mockDao = mock(DerbyDao.class);
		servlet.setDao(mockDao);
		when(mockDao.insertEmail(email)).thenReturn(true);
		assertTrue(servlet.insertIntoDB(email));
	}

}

