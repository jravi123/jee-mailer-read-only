package com.mbcc.examples;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SingletonExample {

	public static void main(String[] args) throws SQLException {
		DataSourceSingleton datasource = DataSourceSingleton.getInstance();

		Connection con = null;
		try {
			con = datasource.getConnection();
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select * from email");
			int count = 1;
			while (rs.next()) {
				System.out.println("Record " + count);
				System.out.println(rs.getString("email_text") + ", " + rs.getString("recipient_category") + ", "
						+ rs.getString("subject"));
				count++;
			}
			rs.close();
			st.close();
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (Exception ignore) {
				}
		}
	}

}
