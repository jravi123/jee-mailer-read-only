package com.mbcc.mailer.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mbcc.mailer.data.DataSourceDao;
import com.mbcc.mailer.data.DerbyDao;

@SuppressWarnings("serial")
public class EmailServlet extends HttpServlet {

	DataSourceDao dao = new DerbyDao();

	void setDao(DataSourceDao dao) {
		this.dao = dao;
	}

	static HashMap<Character, Character> mapper = new HashMap<Character, Character>();

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String initParam = request.getParameter("init");

		if (initParam != null) {
			try {
				dao.createTable();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		ArrayList<EmailDTO> retrievedEmails = dao.getEmails();

		String sendTo = request.getParameter("sendTo");

		if (sendTo != null && sendTo.equals("jsp")) {
			request.setAttribute("emails", retrievedEmails);
			this.getServletContext().getRequestDispatcher("/viewEmail.jsp").forward(request, response);
			return;
		}

		response.setContentType("application.json");

		Gson gson = new Gson();
		String jsonEmails = gson.toJson(retrievedEmails);

		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "GET");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Max-Age", "86400");

		response.getWriter().append(jsonEmails);

	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

		EmailDTO email = new EmailDTO(request.getParameter("subject"), request.getParameter("email_text"),
				request.getParameter("recipient_category"), request.getParameter("tester_email"));

		boolean success = insertIntoDB(email);

		PrintWriter out = response.getWriter();

		String message = "Received your email data and successfully inserted in the database";

		if (!success)
			message = "insert failed";

		out.println(message);

	}

	boolean insertIntoDB(EmailDTO email) {

		return dao.insertEmail(email);
	}

}
