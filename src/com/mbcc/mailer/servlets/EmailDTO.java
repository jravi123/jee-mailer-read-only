package com.mbcc.mailer.servlets;

public class EmailDTO {

	private String subject;
	private String emailText;
	private String recipientCategory;
	private String testerEmail;

	public EmailDTO(String subject, String emailText, String recipientCategory, String testerEmail) {

		if (subject == null)
			throw new IllegalArgumentException("subject cannot be null");

		this.subject = subject;
		
		if (emailText == null)
			throw new IllegalArgumentException("emailText cannot be null");
		
		this.emailText = emailText;
		

		if (recipientCategory == null)
			throw new IllegalArgumentException("Category cannot be null");

		this.recipientCategory = recipientCategory;
		
		this.testerEmail = testerEmail;
	}

	public String getSubject() {
		return subject;
	}

	public String getEmailText() {
		return emailText;
	}

	public String getRecipientCategory() {
		return recipientCategory;
	}

	public String getTesterEmail() {
		return testerEmail;
	}

	@Override
	public String toString() {
		return "EmailDTO [subject=" + subject + ", emailText=" + emailText + ", recipientCategory=" + recipientCategory
				+ ", testerEmail=" + testerEmail + "]";
	}

}
