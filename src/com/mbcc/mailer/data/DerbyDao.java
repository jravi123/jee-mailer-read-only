package com.mbcc.mailer.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mbcc.mailer.servlets.EmailDTO;

public class DerbyDao implements DataSourceDao {

	/**
	 * For DerbyDB you might have to run tomcat with this VM setting for windows
	 * -Dderby.system.home=C:\derby 
	 * as by default derby seems to create files in the restricted system32 folder
	 */

	public boolean insertEmail(EmailDTO email) {

		Connection conn = null;
		Statement stmt = null;

		try {
			Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			conn = DriverManager.getConnection("jdbc:derby:mailer;create=true");

			stmt = conn.createStatement();

			String dbString = "insert into Email (email_text, recipient_category, subject) values ('";
			dbString += email.getEmailText();
			dbString += "', '";
			dbString += email.getRecipientCategory();
			dbString += "', '";
			dbString += email.getSubject();
			dbString += "')";

			stmt.executeUpdate(dbString);

			return true;

		}

		catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
			ex.printStackTrace();
			return false;

		} finally {
			if (stmt != null)
				try {
					stmt.close();

					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}

	}

	public ArrayList<EmailDTO> getEmails() {

		Connection conn = null;
		Statement stmt = null;

		try {
			Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			conn = DriverManager
					.getConnection("jdbc:derby:mailer;create=true;-Dderby.system.home=/users/jayashree/derby;");

			stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery("SELECT * FROM Email");

			ArrayList<EmailDTO> results = new ArrayList<EmailDTO>();

			while (rs.next()) {

				String emailText = rs.getString("email_text");
				String recipientCategory = rs.getString("recipient_category");
				String subject = rs.getString("subject");
				String testerEmail = rs.getString("tester_email");
				EmailDTO email = new EmailDTO(subject, emailText, recipientCategory, testerEmail);
				results.add(email);
			}
			return results;

		} catch (Exception ex) {
			System.out.println(ex);
		}

		return null;

	}

	/** 
	 * call this createTable only once
	 */

	public void createTable() throws SQLException {

		try {
			Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		Connection conn = DriverManager.getConnection("jdbc:derby:mailer;create=true");
		Statement stmt = conn.createStatement();
		try {
			stmt.execute("drop table email");
		} catch (Exception e) {
			// if there is no table it come here but just move on
		}

		String emailTableSql = "CREATE TABLE email (" + "  id int NOT NULL GENERATED ALWAYS AS IDENTITY,"
				+ "  email_text clob NOT NULL," + "  recipient_category varchar(255)  NOT NULL,"
				+ "  subject varchar(255)  NOT NULL," + "  tester_email varchar(255) DEFAULT NULL,"
				+ "  email_date timestamp DEFAULT NULL," + "  PRIMARY KEY (id)" + ")";

		
		stmt = conn.createStatement();
		stmt.execute(emailTableSql);

	}

	public static void main(String[] args) throws SQLException {

		DerbyDao dao = new DerbyDao();
		dao.createTable();
	}

}
