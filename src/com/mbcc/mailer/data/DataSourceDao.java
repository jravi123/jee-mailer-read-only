package com.mbcc.mailer.data;

import java.sql.SQLException;
import java.util.ArrayList;

import com.mbcc.mailer.servlets.EmailDTO;

public interface DataSourceDao {
	
	  boolean insertEmail(EmailDTO email);
	
	  ArrayList<EmailDTO> getEmails();
	  
	  void createTable() throws SQLException;

}
