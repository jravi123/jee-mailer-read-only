package com.mbcc.mailer.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import com.mbcc.mailer.servlets.EmailDTO;

public class MySQLDao implements DataSourceDao {

	// TODO add try-with-resources clause
	// TODO try to use connection pool object for getting connections

	public boolean insertEmail(EmailDTO email) {

		Connection conn = null;
		Statement stmt = null;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		String url = "jdbc:mysql://localhost:3306/mailer";

		Properties info = new Properties();
		info.put("user", "root");
		info.put("password", "Password");
		info.put("serverTimezone", "UTC");

		try {
			conn = DriverManager.getConnection(url, info);

			stmt = conn.createStatement();

			String dbString = "insert into Email (email_text, recipient_category, subject) values ('";
			dbString += email.getEmailText();
			dbString += "', '";
			dbString += email.getRecipientCategory();
			dbString += "', '";
			dbString += email.getSubject();
			dbString += "')";

			stmt.executeUpdate(dbString);

			return true;

		}

		catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
			ex.printStackTrace();
			return false;

		} finally {
			if (stmt != null)
				try {
					stmt.close();

					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

	}

	
	//TODO Resource leak!  None of the db objects are closed!
	public ArrayList<EmailDTO> getEmails() {

		Connection conn = null;
		Statement stmt = null;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			conn = DriverManager
					.getConnection("jdbc:mysql://localhost/mailer?user=root&password=Password&serverTimezone=UTC");

			stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery("SELECT * FROM Email");

			ArrayList<EmailDTO> results = new ArrayList<EmailDTO>();

			while (rs.next()) {

				String emailText = rs.getString("email_text");
				String recipientCategory = rs.getString("recipient_category");
				String subject = rs.getString("subject");
				String testerEmail = rs.getString("tester_email");
				EmailDTO email = new EmailDTO(subject, emailText, recipientCategory, testerEmail);
				results.add(email);
			}
			return results;

		} catch (Exception ex) {
			System.out.println(ex);
		}

		return null;

	}

	public static void main(String[] args) {

		MySQLDao dao = new MySQLDao();
		ArrayList<EmailDTO> emails = dao.getEmails();
		System.out.println(emails);
	}

	@Override
	public void createTable() {
		throw new IllegalAccessError();
		
	}

}
